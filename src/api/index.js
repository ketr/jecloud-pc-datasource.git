/**
 * 用于编写api方法
 * api url 统一在urls.js中声明导出使用，与方法分开
 */
import { ajax } from '@jecloud/utils';
import {
  API_SOURCE_GETPRODUCT,
  API_SOURCE_TREE,
  API_SOURCE_ADD,
  API_SOURCE_UPDATE,
  API_SOURCE_REMOVE,
  API_SOURCE_COPY,
  API_SOURCE_SINGLE_DATA,
  API_TREE_MOVE,
  API_SOURCE_DDL_SQL,
  API_SOURCE_CREATE_DDL,
  API_SOURCE_EXECUTE_SQL,
  API_SOURCE_SQL_FIELDS,
  API_SOURCE_SAVE_POSITION,
  API_SOURCE_PARAMS,
  API_TABLE_GETCOLUMNDATA,
  AP_SOURCE_TABLE_FIELDS,
} from './urls';
/**
 * 获得产品数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getProductData(params) {
  return ajax({ url: API_SOURCE_GETPRODUCT, params: params }).then((info) => {
    if (info) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 根据产品获取左侧表结构树数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getSourceTree(params) {
  return ajax({ url: API_SOURCE_TREE, params: params }).then((info) => {
    if (info != '') {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 数据源添加模块、数据源
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function saveSourceAndModule(params) {
  return ajax({ url: API_SOURCE_ADD, params: params }).then((info) => {
    return info;
  });
}
/**
 * 数据源修改模块、数据源
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function updateSourceAndModule(params) {
  return ajax({ url: API_SOURCE_UPDATE, params: params }).then((info) => {
    return info;
  });
}
/**
 * 数据源删除模块、数据源
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function removeSourceAndModule(params) {
  return ajax({ url: API_SOURCE_REMOVE, params: params }).then((info) => {
    return info;
  });
}
/**
 * 数据源删除模块、数据源
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function copySource(params) {
  return ajax({ url: API_SOURCE_COPY, params: params }).then((info) => {
    return info;
  });
}
/**
 * 获取数据源单条数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getSourceDataById(params) {
  return ajax({ url: API_SOURCE_SINGLE_DATA, params: params }).then((info) => {
    return info;
  });
}

/**
 * 移动树结构排序
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function moveTree(params, headers) {
  return ajax({ url: API_TREE_MOVE, params: params, headers: headers }).then((info) => {
    return info;
  });
}
/**
 * 根据资源表id获取ddl
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getSqlDdlByTableId(params) {
  return ajax({ url: API_SOURCE_DDL_SQL, params: params }).then((info) => {
    return info;
  });
}
/**
 * 根据视图配置生成DDL
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function createDdlByView(params) {
  return ajax({ url: API_SOURCE_CREATE_DDL, params: params }).then((info) => {
    return info;
  });
}
/**
 * 数据源执行操作
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function sourceExecuteSql(params) {
  return ajax({ url: API_SOURCE_EXECUTE_SQL, params: params }).then((info) => {
    return info;
  });
}
/**
 * 数据源执行获取字段
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getExecuteSqlFields(params) {
  return ajax({ url: API_SOURCE_SQL_FIELDS, params: params }).then((info) => {
    return info;
  });
}
/**
 * 视图数据源表位置保存
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function saveSourceTabalPosition(params) {
  return ajax({ url: API_SOURCE_SAVE_POSITION, params: params }).then((info) => {
    return info;
  });
}
/**
 * 获取数据源查询参数
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getSourceParams(params) {
  return ajax({ url: API_SOURCE_PARAMS, params: params }).then((info) => {
    return info;
  });
}
/**
 * 获得表格列数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getColumnData(params) {
  return ajax({ url: API_TABLE_GETCOLUMNDATA, params: params }).then((info) => {
    if (info != '') {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获取资源表数据源表的字段
 * @param {Object} params
 * @return {Promise}
 */
export function getSourceTableFields(params) {
  return ajax({ url: AP_SOURCE_TABLE_FIELDS, params: params }).then((info) => {
    return info;
  });
}
