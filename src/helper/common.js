export const verifyData = {
  name: [
    {
      required: true,
      message: '该输入项为必填项',
    },
    {
      max: 40,
      message: '不能大于40个字符',
    },
    {
      pattern: '^[^\\\\/*?:"\'<>|\x22]+$',
      message: '不能输入非法字符',
    },
  ],
  code: [
    {
      required: true,
      message: '该输入项为必填项',
    },
    {
      max: 40,
      message: '不能大于40个字符',
    },
    {
      pattern: '^[A-Z]{1}[A-Z_0-9]{0,100}$',
      message: '编码由大写字母、下划线、数字组成,且首位为大写字母',
    },
  ],
  lengthMax: {
    max: 40,
    message: '不能大于40个字符',
  },
  required: {
    required: true,
    message: '该输入项为必填项',
  },
};
//数据源类型及描述
export const sourceTypeOpitons = [
  {
    label: '资源表数据源(通过单个资源表获取数据)',
    value: 'TABLE',
  },
  {
    label: '图形数据源(通过图形化的方式快速建立取数规则,仅适合建立简单的取数要求)',
    value: 'VIEW',
  },
  {
    label: 'SQL数据源(通过SQL编辑器编写SQL,无功能限制)',
    value: 'SQL',
  },
  {
    label: 'JS数据源(不支持手机)',
    value: 'JS',
  },
  {
    label: 'Service数据源(通过Spring service实现的数据源)',
    value: 'SERVICE',
  },
  {
    label: '数据接口服务',
    value: 'ACTION',
  },
];
