import { h, ref, toRaw } from 'vue';
import { Modal } from '@jecloud/ui';
import ExecModal from './exec-modal.vue';
export const showModal = ({ execData, props = {} }) => {
  Modal.window({
    title: `结果数据`,
    headerStyle: { height: '50px' },
    bodyStyle: { height: '100%', padding: '0px' },
    content() {
      //execData 执行结果数据
      return h(ExecModal, { execData: execData, ...props });
    },
  });
};
