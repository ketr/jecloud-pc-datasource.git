//返回结果配置项
export class ResultConfig {
  constructor(options) {
    this.fields = options.fields || [];
    this.data = options.data || []; //数据结果
    this.params = options.queryConfig?.map((item) => {
      return new ParamsField(item);
    });
  }
}
//返回结果查询配置项字段
class ParamsField {
  constructor(options) {
    this.name = options.name; //查询参数字段名
    this.label = options.label; //查询参数字段描述
    this.type = options.type; //查询参数类型
  }
}
